<h1 style="text-align: center">Vite with Vue3 and Naive-UI</h1>

<div style="text-align: center">

[![MIT License](https://badgen.net/badge/license/MIT/grey)](https://gitlab.com/CedSharp/vite-naive/blob/main/LICENSE)
[![Typescript 4.7.4](https://badgen.net/badge/icon/v4.7.4/blue?icon=typescript&label)](https://typescriptlang.org)
[![Vite version](https://badgen.net/badge/vite/v3.0.2)](https://vitejs.dev)
[![Vue version](https://badgen.net/badge/vue/v3.0.11)](https://vuejs.org)
[![Last commit](https://badgen.net/gitlab/last-commit/cedsharp/vite-naive)](https://gitlab.com/CedSharp/vite-naive)

</div>

<p style="text-align: center">
    Perfect setup for quick prototyping with a full and powerful UI framework.
</p>

## Documentation

Frequently viewed docs:

- [Vue](https://vuejs.org)
- [NaiveUI](https://naiveui.com)
- [Vite](https://vitejs.org)
- [Typescript](https://typescriptlang.org)
- [Scss](https://sass-lang.com)
- [Eslint](https://eslint.org)
- [Prettier](https://prettier.io)
- [EditorConfig](https://editorconfig.org)
- [Stylelint](https://stylelint.io)
- [Vitest](https://vitest.dev)
- [Husky](https://typicode.github.io/husky/)
- [Lint Staged](https://github.com/okonet/lint-staged)

## Features

- Clone, edit package name, and start coding immediately
- Dark and Light colorscheme support!
- [Eslint](https://eslint.org), [Prettier](https://prettier.io) and [Stylelint](https://stylelint.io) configured with
  best practices
- [Vue3](https://vuejs.org) for templating framework
- [NaiveUI](https://naiveui.com) for amazing set of components
- [Husky](https://typicode.github.io/husky/) to make sure files follow code styles before committing

## Installation

Just clone this repository and install all the packages.

```shell
$ npx degit -m git gitlab:CedSharp/vite-naive
$ git init -b main # don't use default master!
$ npm ci # will also set up husky and git hooks, make sure to setup git first!
```

## Commands

This is basically vite's vue-ts preset with NaiveUI pre-configured and some
added goodies. You can run the project as you would with vite.

There are some additional commands for linting and some for testing.

Finally, there are commands specifically for my editor, which you might
never need.

### Common commands

These commands are common in most vite projects.

```shell
$ npm run dev  # Starts the dev server on http://localhost:3000
$ npm build    # Compiles the project into the "out" directory (vite build)
$ npm start    # Preview a previously build project (vite preview)
$ npm test     # Runs the unit tests
```

### More commands

These commands were added for supporting linting and unit testing.

```shell
$ npm run lint:eslint  # Lints most files for errors
$ npm run lint:tsc     # Lints some more typescript-specific errors
$ npm run lint:sass    # Lints some more scss-specific errors
$ npm run coverage     # In addition to tests, generates a coverage report
$ npm run test:watch   # Keeps running tests while you code
```

### Misc commands

These commands are not meant to be used directly, but instead by IDEs and plugins.

```shell
$ npm run webstorm-integration # This is used by JetBrains editor with the Vitest plugin
$ npm run prepare              # This is used by husky
```

## Recommendations

This starter is meant to add scss to vite's vanilla-ts template, but also
has strong coding style insurance via Eslint, Prettier, EditorConfig and Stylelint.

To get the best experience using this pack, the related plugins should be
installed and configured in the IDE/editor used.

The recommended editors are visual studio code (vscode)
and the IDEs from JetBrains (specifically WebStorm and PhpStorm).

Other editors might also work very well considering those code assistance
tools are very popular.

### Visual Studio Code

Here is a list of recommended plugins to install:

- [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [Eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)

Some of those plugins recommend some specific settings to be set. I recommend
giving their README a look. Here is an example `.vscode/settings.json` file:

```json
{
    "css.validate": false,
    "scss.validate": false,
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "stylelint.validate": [
        "css",
        "scss"
    ],
    "editor.codeActionsOnSave": {
        "source.fixAll": true
    }
}
```

These will disable vscode's default linters, add scss support to Stylelint,
configure Prettier as the main formatter and execute eslint --fix on save.

### WebStorm and PhpStorm

Recommended plugins to install:

- [Prettier](https://plugins.jetbrains.com/plugin/10456-prettier)
- [Vitest Runner](https://plugins.jetbrains.com/plugin/19220-vitest-runner)

WIP - Explain how to configure Eslint, Prettier, Stylelint and Save actions.
