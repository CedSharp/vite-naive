const { spawn } = require('child_process');
const Spinnies = require('terminal-multi-spinners');
const spinnies = new Spinnies();

const errors = {};

const runCommand = (cmd, args) =>
  new Promise((resolve, reject) => {
    const hash = args[1];
    let stdout = '';
    let stderr = '';

    spinnies.add(hash, { text: hash });

    const child = spawn(cmd, [...args, '--colors']);
    child.stdout.on('data', data => (stdout += data));
    child.stderr.on('data', data => (stderr += data));
    child.on('exit', code => {
      if (code !== 0) {
        spinnies.fail(hash);
        errors[hash] = [
          stdout
            .split('\n')
            .filter(l => l)
            .join('\n'),
          stderr
            .split('\n')
            .filter(l => l)
            .join('\n'),
        ];
        reject();
      } else {
        spinnies.succeed(hash);
        resolve();
      }
    });
  });

const run = () => {
  const args = ['lint:eslint', 'lint:tsc', 'lint:sass', 'coverage'];

  return Promise.allSettled(
    args.map(arg => runCommand('npm', ['run', arg]))
  ).then(tasks => {
    let success = tasks.reduce(
      (success, { status }) => (status === 'fulfilled' ? success : false),
      true
    );
    for (let [index, { status }] of tasks.entries()) {
      if (status !== 'fulfilled') {
        if (args[index] in errors) {
          console.error(`\n===[ Error in ${args[index]} ] ===================`);
          console.error(errors[args[index]].join('\n'));
        }
      }
    }
    process.exit(success ? 0 : 1);
  });
};

run().then();
