import { describe, expect, it } from 'vitest';

describe('example component', () => {
  it('should render', () => {
    const host = document.createElement('div');
    document.body.appendChild(host);
    expect(host.outerHTML).toBe('<div></div>');
  });

  it('should support appending a child', () => {
    const host = document.createElement('div');
    const child = document.createElement('p');
    document.body.appendChild(host);
    host.appendChild(child);
    expect(host.outerHTML).toBe('<div><p></p></div>');
  });
});
